import React, {Component} from 'react';
import Button from './components/buttons/Buttons';
import Burger from "./components/Burger-components/Burger";
import Fill from "./components/Burger-components/Fill";

import meatImage from './assets/meat.jpeg';
import cheeseImage from './assets/cheese.jpeg';
import saladImage from './assets/salad.jpeg';
import baconImage from './assets/bacon.jpeg';

import './App.css';


class App extends Component {
    state = {
        price: 20,
        ingredients: [
            {name: 'Meat', count: 0, price: 50},
            {name: 'Cheese', count: 0, price: 20},
            {name: 'Salad', count: 0, price: 5 },
            {name: 'Bacon', count: 0, price: 30}
        ]
    };

    addElement = (key) => {
        let ingredients = [...this.state.ingredients];
        let ingredient = ingredients[key];
        ingredient.count++;
        let price = this.state.price + ingredient.price;
        ingredients[key] = ingredient;
        this.setState({price, ingredients});

    };

    removeElement = (key) => {
        let ingredients = [...this.state.ingredients];
        let ingredient = ingredients[key];
        if (ingredient.count > 0) {
            ingredient.count--;
            let price = this.state.price - ingredient.price;
            ingredients[key] = ingredient;
            this.setState({price, ingredients});
        } else {
            alert('It is impossible to delete zero product');
        }

    };

    showFills = (ingredients) => {
        let fillArr = [];
        for (let i = 0; i < ingredients.length; i++){
            for (let j = 0; j <ingredients[i].count; j++) {
                fillArr.push(<Fill name={ingredients[i].name}/>);
            }
            }
            return fillArr;
        };
        
    render() {

        const Ingredients = [
            {name: 'Meat', image: meatImage},
            {name: 'Cheese', image: cheeseImage},
            {name: 'Salad', image: saladImage},
            {name: 'Bacon', image: baconImage},
        ];

        return (
            <div className="App">
                <div className="Menu">
                    {Ingredients.map((item, key) =>
                        <Button
                            key={key}
                            name={item.name}
                            image={item.image}
                            onClick={() => this.addElement(key)}
                            count={this.state.ingredients[key].count}
                            delete={() => this.removeElement(key)}
                        />
                    )
                    }
                </div>
                <Burger price={this.state.price}>
                    {this.showFills(this.state.ingredients)}
                </Burger>
            </div>
        );
    }
}

export default App;
