import React from "react";

const Burger = props => {
    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {props.children}
            <div className="BreadBottom"></div>
            <p>Price: {props.price}</p>

        </div>
    )
};

export default Burger;