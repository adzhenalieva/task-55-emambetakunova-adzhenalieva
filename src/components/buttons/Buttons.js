import React from "react";

const Button = props => {
    return (
        <div className="button">
            <div className="button btn-inner" onClick={props.onClick}>
                <img src={props.image} alt=""/>
                <span>{props.name}</span>
                <span>{props.count}</span>
            </div>
            <button className="btnDelete" onClick={props.delete}>X</button>
        </div>
    )

};
export default Button;